module Slice
open System
open System.Collections.Generic

[<Literal>]
let Guanine = 'G'
[<Literal>]
let Cytosine = 'C'
[<Literal>]
let Thymine = 'T'
[<Literal>]
let Adenine = 'A'

[<Struct>]
type Orientation =
    Forward
    | Reverse
    
[<Struct>]
type Strand =
    Normal
    | Complement

let complementChar (character : char) : char =
    match character with
    | Guanine -> Cytosine
    | Cytosine -> Guanine
    | Thymine -> Adenine
    | Adenine -> Thymine
    | x -> x

[<Struct>]
[<NoComparison>]
[<CustomEquality>]
type Slice = { orientation: Orientation; strand: Strand; original: char []; start: int; length: int; hash : int } with
    override this.GetHashCode() = this.hash
    override this.Equals(obj) =
        match obj with
        | :? Slice as other -> 
            if this.hash = other.hash &&  
                this.orientation = other.orientation && 
                this.strand = other.strand &&
                this.original = other.original &&
                this.start = other.start &&
                this.length = other.length then
                    true
            else
                    
                    false
        | _ -> false

    override this.ToString() : string =
        this |> Slice.enumerate |> Array.ofSeq |> String
        
    member private this.lastOriginalIndex : int = this.start + this.length - 1

    member this.lastIndex : int = this.length - 1
    
    member slice.item (position: int) : char =
        if position < 0 then 
            ArgumentOutOfRangeException("Position must not be negative")
            |> raise
        if position >= slice.length then 
            ArgumentOutOfRangeException(sprintf "Indexing out of slice. Requested position: %d last valid position: %d" position (slice.length - 1) )
            |> raise
        let transform =
            match slice.strand with
            | Normal -> id
            | Complement -> complementChar
        match slice.orientation with
        | Forward ->
            slice.original |> Array.item (position + slice.start ) |> transform
        | Reverse ->
            slice.original |> Array.item (slice.start + slice.length - 1 - position) |> transform
            
    static member computeHash (slice : Slice) =
        let mutable hash = 0
        for idx in [0..slice.lastIndex] do
            hash <- hash * 31 + (slice.item idx |> int)  
        hash

    static member create (start: int, length: int, orientation : Orientation, strand: Strand) (original : char []) : Slice = 
        if start < 0 then raise <| ArgumentOutOfRangeException("Position must not be negative")
        if start + length - 1 > original.Length then raise <| ArgumentOutOfRangeException("Slice would fall out of original array")
        let tmp = { orientation = orientation; strand = strand; original = original; start = start; length = length; hash = 0}
        let hash = tmp |> Slice.computeHash
        { tmp with hash = hash}

    static member createForward (start: int, length: int) (original : char []) : Slice =
        original |> Slice.create (start, length, Forward, Normal)
    
    static member create2 (original : char[]) : Slice =
        original |> Slice.create (0, original.Length, Forward, Normal)
    
    static member fromSlice (start: int, length: int) (original: Slice) : Slice =
        match original.orientation with
        | Forward ->
            let newStart = original.start + start
            if newStart > original.lastOriginalIndex then ArgumentOutOfRangeException() |> raise
            if newStart + length - 1 > original.lastOriginalIndex then ArgumentOutOfRangeException() |> raise
            original.original |> Slice.create (original.start + start, length, Forward, original.strand)
        | Reverse -> 
            let newStart = original.start + original.length - start - length
            if newStart > original.lastOriginalIndex then ArgumentOutOfRangeException() |> raise
            original.original |> Slice.create (newStart, length, Reverse, original.strand)
        
    static member enumerate (slice : Slice) : seq<char> =
        seq {
            for i in [0..slice.length - 1] do
                yield slice.item i
        }
           
    static member reverse (slice : Slice) : Slice =
        match slice.orientation with
        | Forward -> { slice with orientation = Reverse }
        | Reverse -> { slice with orientation = Forward }
        
    static member complement (slice : Slice) : Slice =
        match slice.strand with
        | Normal -> { slice with strand = Complement }
        | Complement -> { slice with strand = Normal }
    
    static member reverseComplement : Slice -> Slice = Slice.reverse >> Slice.complement
    
    interface IEnumerable<char> with
        member x.GetEnumerator() : IEnumerator<char> = (x |> Slice.enumerate).GetEnumerator()
        
    interface Collections.IEnumerable with
        member this.GetEnumerator() = (this :> IEnumerable<char>).GetEnumerator() :> Collections.IEnumerator
        