module Statistics
open FastQ


let formatAlignResultWithHeader (input: seq<string>) =
    seq {
        yield "min_overlap_size,min_quality,id,lane,tile,x,y,aligned,fragment_length,overlap_size,overlap_quality"
        for line in input do
            yield line
    }

let formatAlignResult ((overlapSize, minQuality) : int * byte) ((pair, alignResult) : FastQRecordPair * AlignmentResult) =
    let readId = pair.readId
    let formatter = sprintf "%d,%d,%d,%d,%d,%d,%d,%b,%d,%d,%d"
                                        overlapSize
                                        minQuality 
                                        pair.readNumber 
                                        readId.lane 
                                        readId.tile 
                                        readId.x 
                                        readId.y
    match alignResult with
    | NotAligned -> formatter false -1 -1 -1
    | Aligned(start) -> 
            let overlapSize = pair.first.sequence.length - start
            let fragmentLength = start + pair.second.sequence.length
            let numOfMatchingBases = pair |> FastQ.countMatchingBases start
            formatter true fragmentLength overlapSize numOfMatchingBases
        