module FastQ

open System
open System.IO
open System.Text

open Slice

let qualityScoreBase : byte = byte '!'
type ReadNumber = int
type ReadId = { instrument: string; runNumber: int; flowCellId: string; lane: int; tile: int; x: int; y: int }
type FastQRecord = { id: string; sequence : Slice; quality: Slice }
type FastQRecordPair = { readNumber: ReadNumber; readId: ReadId; first: FastQRecord; second: FastQRecord }
type AlignmentResult = NotAligned | Aligned of int
type SequenceWithQuality = { sequence: Slice; quality: Slice }
[<RequireQualifiedAccess>]
type QualityTrimResult = Success of int * int | Failure


module ReadId =
    let parse (line: string) : ReadId =
        let split = line.Split(' ', StringSplitOptions.RemoveEmptyEntries)
        let split = split.[0].Split(':', StringSplitOptions.RemoveEmptyEntries)
        { 
            instrument = split.[0].Substring(1)
            runNumber = int(split.[1])
            flowCellId = split.[2]
            lane = int(split.[3])
            tile = int(split.[4])
            x = int(split.[5])
            y = int(split.[6])
        }
        
    let filterByLane (laneId: int) (readId: ReadId) : bool =
        readId.lane = laneId
        
    let filterByTile (tileId: int) (readId: ReadId) : bool =
        readId.tile = tileId 

let minusBaseQuality qualityValue = qualityValue - qualityScoreBase

let isQualityTrimRequired (minimumQuality: byte) (quality: Slice) : QualityTrimResult =
    let mutable forwardIndex = 0
    while forwardIndex <= quality.lastIndex && 
        (forwardIndex |> quality.item |> byte |> minusBaseQuality < minimumQuality) do
        forwardIndex <- forwardIndex + 1
        
    if forwardIndex > quality.lastIndex then
        QualityTrimResult.Failure
    else    
        let mutable reverseIndex = quality.lastIndex
        while reverseIndex >= 0 &&
            (quality.item reverseIndex |> byte |> minusBaseQuality < minimumQuality) do
            reverseIndex <- reverseIndex - 1
        
        if reverseIndex <> -1 then
            QualityTrimResult.Success(forwardIndex, reverseIndex)
        else
            QualityTrimResult.Failure

let qualityTrimFastQRecord (minScore: byte) (record: FastQRecord) : FastQRecord =
    
    match record.quality |> isQualityTrimRequired minScore with
    | QualityTrimResult.Failure -> record
    | QualityTrimResult.Success(firstIndex, lastIndex) ->
        let length = lastIndex - firstIndex + 1
        { record with
            sequence = record.sequence |> Slice.fromSlice (firstIndex, length)
            quality = record.quality |> Slice.fromSlice (firstIndex, length ) }

let qualityTrimFastqRecordPair (minScore: byte) (pair: FastQRecordPair) : FastQRecordPair =
    { pair with
        first = pair.first |> qualityTrimFastQRecord minScore
        second = pair.second |> qualityTrimFastQRecord minScore
    }

let readFastqRecords (streamReader: StreamReader) : seq<FastQRecord> =
    seq {
        while not streamReader.EndOfStream do 
            let id = streamReader.ReadLine().Substring(1)
            let sequence = streamReader.ReadLine().ToCharArray()
            let ``ignored line with plus character`` = streamReader.ReadLine()
            let quality = streamReader.ReadLine().ToCharArray()
            if quality.Length <> sequence.Length then raise <| ArgumentOutOfRangeException("Quality length does not equal to sequence length")
            let sequenceSlice = sequence |> Slice.create2
            let qualitySlice = quality |> Slice.create2
            yield { id = id ; sequence = sequenceSlice; quality = qualitySlice}
    }
    
let readFastqRecordPairs (streamReader : StreamReader) (streamReader' : StreamReader) : seq<FastQRecordPair> =
    let reads = readFastqRecords streamReader
    let readPairs = readFastqRecords streamReader'
    readPairs 
    |> Seq.zip reads
    |> Seq.mapi (fun idx (first, second) -> { readNumber = idx * 2 ; readId = first.id |> ReadId.parse ; first = first; second = second })

let reverseComplementSecondSequence (pair: FastQRecordPair) =
    { 
    pair with second = 
                { pair.second with 
                    sequence = pair.second.sequence |> Slice.reverseComplement; 
                    quality = pair.second.quality |> Slice.reverse 
                }
    }

let reverseComplementSecondSequences (pairs : seq<FastQRecordPair>) : seq<FastQRecordPair> =
    seq { 
        for pair in pairs do
            yield pair |> reverseComplementSecondSequence
    
    }    
    
let yieldAlignedRecords (records : seq<FastQRecordPair * AlignmentResult>) : seq<string> =
    seq {
        for (pair, matchResult) in records do
            match matchResult with
            | Aligned(start) ->
                yield pair.readNumber |> string
                yield pair.first.sequence.ToString()
                yield pair.second.sequence.ToString().PadLeft(start + pair.second.sequence.length)
                yield String.Empty
            | NotAligned ->
                yield pair.readNumber |> string
                yield "Not aligned"
                yield String.Empty
    }

let alignForward (minimumMatchScore : int) (pair: FastQRecordPair) : AlignmentResult =
    let mutable result = NotAligned
    let firstSequence = pair.first.sequence
    let secondSequence = pair.second.sequence
    let lastIndexOfFirst = firstSequence.lastIndex
    let lastIndexOfSecond = secondSequence.lastIndex
    let mutable firstIndex = 0
    let mutable matchFound = false
    
    while firstIndex <= lastIndexOfFirst && not matchFound do
        let mutable secondIndex = 0
        let mutable hitCount = 0
        while secondIndex <= lastIndexOfSecond &&
                firstIndex + hitCount <= lastIndexOfFirst &&
                hitCount <= minimumMatchScore && 
                firstSequence.item (firstIndex + hitCount) = (secondSequence.item secondIndex)  do
            hitCount <- hitCount + 1
            secondIndex <- secondIndex + 1
        if hitCount > minimumMatchScore then
            matchFound <- true
            result <- Aligned(firstIndex)
        
        firstIndex <- firstIndex + 1
    result
    
let alignBackward (minimumMatchScore : int) (pair: FastQRecordPair) : AlignmentResult =
    let mutable result = NotAligned
    let firstSequence = pair.first.sequence
    let secondSequence = pair.second.sequence
    let lastIndexOfFirst = firstSequence.lastIndex
    let lastIndexOfSecond = secondSequence.lastIndex
    let mutable firstIndex = lastIndexOfFirst - minimumMatchScore
    let mutable matchFound = false
    
    while firstIndex >= 0 && not matchFound do
        let mutable secondIndex = 0
        let mutable hitCount = 0
        while secondIndex <= lastIndexOfSecond &&
                hitCount <= minimumMatchScore && 
                firstSequence.item (firstIndex + hitCount) = (secondSequence.item secondIndex)  do
            hitCount <- hitCount + 1
            secondIndex <- secondIndex + 1
        if hitCount > minimumMatchScore then
            matchFound <- true
            result <- Aligned(firstIndex)
        
        firstIndex <- firstIndex - 1
    result
    
let align (alignerMethod: FastQRecordPair -> AlignmentResult) (pair : FastQRecordPair ) =
    pair, pair |> alignerMethod
    
let countMatchingBases (startIndex: int) (pair: FastQRecordPair) =
    let overlapSize = Math.Min(pair.first.sequence.length - startIndex, pair.second.sequence.length)
    let sequence = pair.first.sequence
    let sequence' = pair.second.sequence
    let mutable matchingBases = 0
    for index in [0..overlapSize - 1] do
        if sequence.item (startIndex + index) = 
            (sequence'.item index) then
            matchingBases <- matchingBases + 1
    matchingBases
    
let selectReadPair (number : ReadNumber) (pair : FastQRecordPair) : bool = pair.readNumber = number

let filterByTile (tileId: int) (pair: FastQRecordPair) : bool = pair.readId |> ReadId.filterByTile tileId

let isMatch (result : AlignmentResult) : bool =
    match result with
    | NotAligned -> false
    | Aligned(_) -> true
    
let mergeAlignedRecords (pair: FastQRecordPair) (startPosition: int) =
    let newLength = pair.first.sequence.length - startPosition - 1 + pair.second.sequence.length
    let newArray = Array.init newLength (fun _ -> 'Z')
    let mutable index = 0
    while index < startPosition do
        newArray.[index] <- pair.first.sequence.item index
        index <- index + 1
    let mutable secondIndex = 0
    while index < pair.first.sequence.length do
        newArray.[index] <- pair.first.sequence.item index
        index <- index + 1
        secondIndex <- secondIndex + 1
    while secondIndex < pair.second.sequence.length do
        newArray.[index] <- pair.second.sequence.item secondIndex
        index <- index + 1
        secondIndex <- secondIndex + 1
    newArray |> Slice.create2