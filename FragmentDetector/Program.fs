﻿open System
open System.IO
open System.IO.Compression
open System.Text

open FastQ
open Statistics
open System.Diagnostics  

[<EntryPoint>]
let main argv =
    let overlapSizes = [5..5..20]
    let minQualities = [25uy..35uy] |> Seq.append [0uy]
    let tile = 1101
    let parameterCombinations = minQualities |> Seq.collect (fun minQuality -> overlapSizes |> Seq.map (fun overlapSize -> overlapSize, minQuality))

    //let dumpReads = FastQ.yieldAlignedRecords >> (fun lines -> File.WriteAllLines("/home/legezam/tmp/fragments/records_merged_trim_28.txt", lines))
    
    use streamReader = new StreamReader(new FileStream("/home/legezam/tmp/IHW01004-POOL-050415S-113_S17_L001_R1_001.fastq", FileMode.Open))
    use streamReader' = new StreamReader(new FileStream("/home/legezam/tmp/IHW01004-POOL-050415S-113_S17_L001_R2_001.fastq", FileMode.Open))
    let originalReads = FastQ.readFastqRecordPairs streamReader streamReader'
                                            //|> Seq.filter (FastQ.filterByTile tile)
                                            |> Array.ofSeq
    let linesOfLines =
        seq {
            for overlapSize, minQuality in parameterCombinations do
                printfn "Calculating size=%d, quality=%d" overlapSize minQuality
                yield originalReads
                            |> Seq.map (FastQ.qualityTrimFastqRecordPair minQuality)
                            |> Seq.map FastQ.reverseComplementSecondSequence
                            |> Array.ofSeq
                            |> Seq.map (FastQ.align <| FastQ.alignBackward overlapSize) 
                            |> Seq.map (formatAlignResult (overlapSize, minQuality))
                            
        }
        
    File.WriteAllLines("/home/legezam/tmp/fragments/result.txt", linesOfLines |> Seq.collect id |> formatAlignResultWithHeader)
    0
